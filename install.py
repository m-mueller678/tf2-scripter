#!/usr/bin/env python3

from os.path import expanduser

INSTALL_PREFIX='overrides/'

# reload is for tele wrench!

def user_content():
    not_vaccinator=[s for s in get_states() if s.tf_class != 'medic' or s.itemset != 3]
  
    bind(not_vaccinator,'q','+use_action_slot_item')
    bind(not_vaccinator,'r','+'+create_toggle('+reload; voicemenu 2 2','-reload'))

    bind(get_states('medic',3),'q','+reload')
    bind(get_states('medic',3),'r','voicemenu 2 2')
    
    bind(get_states('medic'),'f','slot3')
    bind(get_states('medic'),'mouse4','+'+create_toggle('slot1','slot2'))
    bind(get_states('medic'),'mouse5','+'+create_toggle('hud_medicautocallersthreshold 500','hud_medicautocallersthreshold 80'))
    bind(get_states('medic'),'f3','voicemenu 1 7')
    
    bind(get_states('medic',3),'MWHEELDOWN','+reload')
    
    
    bind(get_states('sniper',0),'f','slot3')
#    bind(get_states('sniper',0),'mouse2','+'+create_toggle('slot2;+attack;+attack2','-attack;-attack2;slot1'))
    bind(get_states('sniper',1),'f','slot3')
#    bind(get_states('sniper',1),'mouse2','+'+create_toggle('slot2;+attack;+attack2','-attack;-attack2;slot1'))
    bind(get_states('sniper',2),'f','slot3')
#    bind(get_states('sniper',2),'mouse2','+'+create_toggle('slot2;+attack;+attack2','-attack;-attack2;slot1'))
    
    bind(get_states('engineer'),'mouse4','+'+create_toggle('destroy 2 0; build 2 0;','lastinv'))
    bind(get_states('engineer'),'4','destroy 0 0; build 0 0;')
    bind(get_states('engineer'),'5','destroy 1 0; build 1 0;')
    bind(get_states('engineer'),'6','destroy 1 1; build 1 1;')
    
#    bind(get_states('demoman',0),'v','+'+create_toggle('+attack2; slot3; wait 10; -attack2','load_itempreset 0'))
#    bind(get_states('demoman',1),'v','+'+create_toggle('+attack2; slot3; wait 10; -attack2','load_itempreset 1'))
    bind(get_states('demoman',1),'f','slot3')
    bind(get_states('demoman',1),'mouse4','slot1')
    
    bind(get_states('soldier'),'mouse2','+'+create_toggle('slot2;+attack','slot1;-attack'))
    bind(get_states('soldier'),'mouse4','+'+create_toggle('slot3','slot1'))
    
    bind(get_states('pyro'),'f','slot3')
    bind(get_states('pyro'),'mouse4','+'+create_toggle('slot2','slot1'))
    
    bind(get_states('heavyweapons'),'mouse4','+'+create_toggle('slot2','slot1'))
    bind(get_states('heavyweapons'),'f','slot3')

# 3.75 @ 1200 dpi
    run_on_enter(get_states(),to_simple_command('viewmodel_fov 60; fov_desired 90; tf_use_min_viewmodels 1; sensitivity 3.75'))

##################
# edit above
##################

TF2_PATH=expanduser("~/.steam/steam/steamapps/common/Team Fortress 2/tf/cfg/")
ALL_CLASSES={"engineer","demoman","heavyweapons","medic","pyro","scout","sniper","soldier","spy"}
ALL_KEYS=set().union(
    map(chr,range(ord('a'),ord('z')+1)),
    map(lambda i:'f'+str(i),range(1,13)),
    #map(str,range(0,10)),
    map(lambda i:'mouse'+str(i),range(1,6)),
    map(lambda i:str(i),range(0,10)),
    {'space','MWHEELDOWN','MWHEELUP'},
)

ALL_EVENTS=set().union(
    map(lambda c:'class_'+c,ALL_CLASSES),
    map(lambda i:'itemset_'+str(i),range(4)),
)
all_states={}

commands_by_content={}
commands_by_id=[]

toggle_count=0

# directly usable in alias or bind without quoting
def to_simple_command(cmd,force_alias=False):
    if (' ' not in cmd) and (';' not in cmd) and (not force_alias):
        return cmd
    assert '"' not in cmd and "'" not in cmd
    if cmd in commands_by_content:
        return "c"+str(commands_by_content[cmd])
    else:
        cid=len(commands_by_id)
        commands_by_id.append(cmd)
        commands_by_content[cmd]=cid
        return 'c'+str(cid)

def create_toggle(start,end):
    global toggle_count
    tid='t'+str(toggle_count)
    toggle_count+=1
    dump_cmd(concat_cmd('alias','+'+tid,'"'+to_simple_command(start)+'"'))
    dump_cmd(concat_cmd('alias','-'+tid,'"'+to_simple_command(end)+'"'))
    return tid

def dump_commands():
    global commands_by_id
    for i in range(len(commands_by_id)):
        dump_cmd(concat_cmd('alias','c'+str(i),'"'+commands_by_id[i]+'"'))
    commands_by_id=None

def make_bind_command(key,cmd):
    assert(key in ALL_KEYS)
    return "bind "+key+" "+to_simple_command(cmd)

class State:
    def __init__(self,tf_class,itemset):
        assert(tf_class in ALL_CLASSES)
        assert(itemset in {0,1,2,3})
        self.name=tf_class+str(itemset)
        self.tf_class=tf_class
        self.itemset=itemset
        self.binds={}
        self.transitions={}
        self.commands=[]
    
    def alias_name(self):
        return 'state_'+self.name
    
    def get_num_key_bind(self,x):
        return self.binds.get(str(x),'slot'+str(x))
    
    def on_enter(self):
        commands=list(self.commands)
        for key in self.binds:
            commands.append(make_bind_command(key,self.binds[key]))
        for event in ALL_EVENTS:
            if event in self.transitions:
                commands.append(concat_cmd('alias',event,self.transitions[event].alias_name()))
            else:
                commands.append(concat_cmd('alias',event,to_simple_command('echo no transition')))
        return command_chain(commands)

def get_state(tf_class,itemset):
    return all_states[State(tf_class,itemset).name]

def run_on_enter(states,cmd):
    for s in states:
        s.commands.append(cmd)

def bind(states,key,cmd):
    for s in states:
        assert(key in ALL_KEYS)
        assert key not in s.binds,key + ' in '+ s.name
        s.binds[key]=cmd

def transition(states,event,state_map):
    for s in states:
        new_state=state_map(s)
        assert(event in ALL_EVENTS)
        assert(new_state.name in all_states)
        assert(event not in s.transitions)
        s.transitions[event]=new_state;

def command_chain(cmds):
    concatenated=''
    for c in cmds:
        concatenated=concatenated+c+';'
        if len(concatenated)>200:
            concatenated=to_simple_command(concatenated[:-1],force_alias=True)+';'
    return concatenated[:-1]

def concat_cmd(*components):
    for c in components:
        if c[0]=='"':
            assert(c[-1]=='"')
            assert('"' not in c[1:-1])
        else:
            assert('"' not in c)
            assert(' ' not in c)
            assert(';' not in c)
    return ' '.join(components)

def dump_cmd(c):
    autoexec_file.write(c+'\n')

def dump():
    for s in all_states.values():
        dump_cmd(concat_cmd('alias',s.alias_name(),to_simple_command(s.on_enter())))
    dump_commands()

def get_states(class_filter=None,itemset_filter=None):
    if class_filter is not None:
        assert(class_filter in ALL_CLASSES or callable(class_filter))
    if itemset_filter is not None:
        assert(isinstance(itemset_filter,int) and 0<=itemset_filter<4 or callable(itemset_filter))
    for s in all_states.values():
        if all([
            callable(class_filter) and class_filter(s.tf_class) or s.tf_class==class_filter or class_filter is None,
            callable(itemset_filter) and itemset_filter(s.itemset) or s.itemset==itemset_filter or itemset_filter is None,
        ]):
            yield s

def bind_itemsets():
    keys = ['kp_home','kp_uparrow','kp_pgup','kp_leftarrow']
    return command_chain( [ concat_cmd('bind',keys[i],'itemset_'+str(i)) for i in range(4)] )


for c in ALL_CLASSES:
    with open(TF2_PATH+INSTALL_PREFIX+c+'.cfg','w') as f:
        f.write('class_'+c)
    for i in range(4):
        s=State(c,i)
        s.commands.append(concat_cmd('load_itempreset',str(i)))
        all_states[s.name]=s

for i in range(4):
    transition(get_states(),'itemset_'+str(i),lambda old: all_states[State(old.tf_class,i).name])
for c in ALL_CLASSES:
    transition(get_states(),'class_'+c,lambda old: all_states[State(c,0).name])

autoexec_file=open(TF2_PATH+INSTALL_PREFIX+'autoexec.cfg','w')
run_on_enter(get_states(),to_simple_command('exec '+INSTALL_PREFIX+'default_binds;' + bind_itemsets(),force_alias=True))
user_content()
dump()
dump_cmd('alias fix_health "tf_hud_target_id_disable_floating_health 0; tf_hud_target_id_offset  0; tf_healthicon_height_offset 10; tf_hud_target_id_disable_floating_health 1"')
dump_cmd('state_medic0')
dump_cmd('join_class medic')
autoexec_file.close()

for s in get_states():
  print(s.name,s.binds)
